---
draft: false
title: "Chisapapo"
author: Nenita
recipe_image: images/recipe-headers/chisapapo.png
date: 2023-05-25T16:40:54+01:00
tags: ["cena"]
tagline: "Chorizo, Papa y Pollo"
calories:
servings: 2
cook: true
cook_increment: minutes
prep_time: 20
cook_time: 20

---

## Video

{{< youtube YiSJnYWeQf8 >}}

## Ingredientes

- Chorizo o salchicha grande - 1
- Papas grandes - 2
- Muslo de pollo - 1
- Cebolla - 1/2
- Aceite de oliva
- Caldo de pollo
- Vino blanco [^1]
- Especias (laurel, cayena, pimienta, romero)
- Perejil

## Instrucciones

1. Preparación
    1. Chorro de aceite a la sartén
    1. Cortar papas en cuartos y colocar con parte plana hacia abajo
    1. Cortar salchicha/chorizo y pollo en trozos similares y colocar en contacto con la sartén
    1. Añadir cebolla finamente picada
    1. Salpimentar y especiar
1. Cocción
    1. Fuego alto hasta dorar bien las papas
    1. Tapar y cocinar a fuego medio hasta que las papas comiencen a hacerse
    1. Girar papas, pollo y salchicha/chorizo
    1. Añadir un chorro de vino y esperar a que se evapore todo el alcohol
    1. Añadir caldo de pollo al gusto
    1. Dejar reducir destapado y a fuego medio al gusto [^2]

#### Notas

[^1]: Vino de beber, no de cocinar
[^2]: Tapar para cocinar más/más rápido las papas
