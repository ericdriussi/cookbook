---
draft: false
title: "Boñolesa"
author: Nenito
recipe_image: images/recipe-headers/ragu.jpg
date: 2023-05-21T17:38:22+01:00
tags: ["pasta", "salsa"]
tagline: "La boñolesa pa la pasta"
servings:
prep_time: 30
cook_time: 120
calories:
cook: true
cook_increment: minutes

---

## Ingredientes

- Carne picada (mixta) - 1Kg
- Cebolla - 1/2
- Zanahorias - 150g
- Panceta - 100g
- Tomate natural triturado - 1200ml
- Ketchup - 1 cucharadita
- Aceite de oliva
- Vino tinto [^1]
- Especias (comino, laurel, orégano, pimienta, albahaca)

## Instrucciones

1. Preparar fritura
    1. Cocinar la panceta a fuego medio con un chorro de aceite de oliva
    1. Picar zanahorias y cebolla (finas)
    1. Agregar a la panceta y pochar a fuego lento
    1. Sacar de la hoya y reservar para después
1. Cocinar la carne
    1. Cocinar la carne a fuego alto, removiendo y separando hasta que comienze a dorar y el jugo haya evaporado
    1. Añadir un chorro de vino y esperar a que se evapore todo el alcohol
    1. Añadir fritura y salpimentar
1. Cocinar la salsa
    1. Añadir el tomate triturado y el ketchup y especiar
    1. Cocinar a fuego lento hasta conseguir la textura deseada (2h al menos)

#### Notas

[^1]: Vino de beber, no de cocinar
