---
draft: false
title: "Title for your recipe"
author: {{ .Site.Params.front.defaultAuthor | default "User" }}
recipe_image: {{ .Site.Params.front.defaultImage | default "images/defaultImage.png" }}
date: {{ .Date }}
tags: ["tag1", "tag2"]
tagline: "A short tagline for your recipe"
calories: 300
servings: 4
cook: true
cook_increment: minutes
prep_time: 15
cook_time: 8

---

## Ingredientes

#### Sub-categoría

- First Ingredient
- Second Ingredient [^1]
- Third Ingredient
- Fourth Ingredient
- Fifth Ingredient

## Instrucciones

1. Step One
   1. Sub Step One
1. Step Two
1. Step Three
1. Step Four
1. Step Five
1. Step Six

#### Notas

[^1]: Footnote 1
